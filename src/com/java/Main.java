package com.java;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

public class Main {

    public static void main(String[] args) {
	    String date = "20190101123000";

	    try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
            Date afterDate = simpleDateFormat.parse("20190101123000");

            GregorianCalendar cal = new GregorianCalendar();
            cal.setTime(afterDate);

            XMLGregorianCalendar xmlGregCal =  DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);
            String result = simpleDateFormat.format(xmlGregCal.toGregorianCalendar(TimeZone.getTimeZone("GMT+09:00"), Locale.KOREA, xmlGregCal).getTime());
            System.out.println(result);
        } catch (Exception e) {

        }
    }
}
